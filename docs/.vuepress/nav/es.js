module.exports = [{
    text: 'Inicio',
    link: '/es/'
},
{
    text: 'Manual',
    link: '/es/pbst/'
},
{
    text: 'Créditos',
    link: '/es/credits/'
},
{
    text: 'Pinewood',
    items: [{
            text: 'Manual del PET',
            link: 'https://pet.pinewood-builders.com'
        },
        {
            text: 'Manual del TMS',
            link: 'https://tms.pinewood-builders.com'
        }
    ]
}

]